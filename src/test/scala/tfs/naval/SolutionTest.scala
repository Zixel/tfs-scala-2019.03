package tfs.naval

import tfs.naval.field.MockFieldController
import tfs.naval.Main
import tfs.naval.Solution

import org.scalatest.{FlatSpec, Matchers}

class SolutionTest extends FlatSpec with Matchers{

  val mockShipController = new ship.MockShipController
  val mockFieldController = new field.MockFieldController
  "ships" should "be linear" in {
    val testShip1 = List(
      (1,1),
      (1,2),
      (1,3)
    )

    val testShip2 = List(
      (1,1)
    )

    mockShipController.validateShip(testShip1) shouldBe true

    mockShipController.validateShip(testShip2) shouldBe true

    val testShip3 = List(
      (1,1),
      (1,2),
      (2,2)
    )



    mockShipController.validateShip(testShip3) shouldBe false

  }

  "Empty ships" should "be rejected" in {
    val testShip = List()

    mockShipController.validateShip(testShip) shouldBe false

  }

  "ships" should "be no longer then 4" in {

    val testShip1 = List(
      (1,1),
      (1,2),
      (1,3),
    )

    val testShip2 = List(
      (1,1)
    )

    mockShipController.validateShip(testShip1) shouldBe true

    mockShipController.validateShip(testShip2) shouldBe true

    val testShip3 = List(
      (1,1),
      (1,2),
      (1,3),
      (1,4),
      (1,5)
    )

    mockShipController.validateShip(testShip3) shouldBe false
  }

  "ships" should "be placed separately" in {
    val field = Vector(
      Vector(false,false,false,false,false),
      Vector(false,true,false,false,false),
      Vector(false,true,false,false,false),
      Vector(false,true,false,false,false),
      Vector(false,false,false,false,false)
    )

    //Валидный корабль
    val ship = List(
      (1,1),
      (1,2)
    )

    mockFieldController.validatePosition(ship, field) shouldBe false

    //Невалидный корабль
    val ship2 = List(
      (4,5),
      (5,5)
    )

    mockFieldController.validatePosition(ship2, field) shouldBe true

  }
}
