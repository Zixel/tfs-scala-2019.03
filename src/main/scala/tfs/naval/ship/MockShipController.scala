package tfs.naval.ship

import tfs.naval.model.Ship

class MockShipController extends ShipController {

  override def validateShip(ship: Ship): Boolean = {

    def noRepeats():Boolean = {
      if(ship.size > ship.distinct.size) false
      else true
    }

    def isStraight(): Boolean ={
      var min_1 = 10
      var max_1 = 0

      var min_2 = 10
      var max_2 = 0

      for(x <- ship) {
        if(x._1 < min_1) min_1 = x._1
        if(x._1 > max_1) max_1 = x._1
        if(x._2 < min_2) min_2 = x._2
        if(x._2 > max_2) max_2 = x._2
      }

      if(min_1 == max_1 || min_2 == max_2) true
      else false
    }

    def hasNoGaps(s: Ship, pred: (Int,Int)):Boolean = {
      (s,pred) match {
        case (List(), (0,0)) => false
        case (List(), _) => true
        case (p::xs, (0,0)) => hasNoGaps(xs,p)
        case (p::xs, (x@_,y@_)) => {
          if((p._1 - x).abs <= 1 && (p._2 - y).abs <= 1 && p != pred) hasNoGaps(xs,p)
          else false
        }
      }
    }

    if(ship.length<=4 && ship.length >= 1 && isStraight && hasNoGaps(ship, (0,0)) && noRepeats){
      true
    } else {
      false
    }
  } // определить, подходит ли корабль по своим характеристикам
}