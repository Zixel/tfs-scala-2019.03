package tfs.naval.fleet

import tfs.naval.model.{Field, Fleet, Ship}

class MockFleetController extends FleetController {

  override def enrichFleet(fleet: Fleet, name: String, ship: Ship): Fleet = {
    if(!fleet.contains(name)) fleet.+((name,ship))
    else fleet
  } // добавить корабль во флот
}
