package tfs.naval.field

import tfs.naval.model.{Field, Ship}

class MockFieldController extends FieldController {

  override def validatePosition(ship: Ship, field: Field): Boolean = {
    def isValid(p: (Int,Int)):Boolean = {
      if((p._1) >= 1 &&
        (p._1) <= field.size &&
        (p._2 ) >= 1 &&
        (p._2) <= field.size &&
        !field.apply(p._1-1).apply(p._2-1)) true //проверка есть ли там вообще корабль
      else false
    }

    def isInShip(p: (Int,Int)):Boolean = ship.contains(p)


    def isNoShipsCloseToBorder(p: (Int,Int)): Boolean = {
      var res = true
      if((p._1) >= 1 && (p._1) <= field.size && (p._2) >= 1 && (p._2) <= field.size){
        for(i <- p._1 - 1 to p._1 + 1) {
          for (j <- p._2 - 1 to p._2 + 1) {
            if(i >= 1 && i <= field.size && j >= 1 && j <= field.size){
              if ((!isInShip(i+1,j+1)) && field.apply(i-1).apply(j-1)) {
                res = false
              }
            }

          }
        }
      }
      res
    }
    ship.forall{p => isValid(p)} & ship.forall(p => isNoShipsCloseToBorder(p))//проверка всего корабля
  } // определить, можно ли его поставить

  override def markUsedCells(field: Field, ship: Ship): Field = {
    var res: Field = field
    ship.map(x => {
      if((x._1 - 1) >= 0 && (x._1 - 1) <= 9 && (x._2 - 1) >= 0 && (x._2 - 1) <= 9){
        res = res.updated(x._1 - 1,res.apply(x._1 - 1).updated(x._2 - 1, true))
      } else {
        field
      }
    })
    res
  } // пометить клетки, которые занимает добавляемый корабль
}
